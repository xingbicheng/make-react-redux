import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class Provider extends Component {
  // Provider只是一个容器
  static propTypes = {
    store: PropTypes.object,
    children: PropTypes.any
  }

  static childContextTypes = { // 声明，想要使用context必须声明
    // 只有声明了childContextTypes才能将xxx放入context
    // 外界可以通过 props 给它提供 store，它会把 store 放到自己的 context 里面，
    // 好让子组件 connect 的时候都能够获取到。
    store: PropTypes.object
  }

  getChildContext () {
    // getChildContext即设置context的过程
    // 该步骤是给将context设置为{ store }
    return {
      store: this.props.store
    }
  }

  render () {
    return (
      <div>{this.props.children}</div>
    )
  }
}

export const connect = (mapStateToProps, mapDispatchToProps) => (WrappedComponent) => {
  // 第二个参数是一个纯组件，返回一个<Connect/>组件，
  // 并在其中设置props，传入的纯组件即为<Connect/>的子组件
  // 所以纯组件可以通过this.props获取connect中的state的内容
  // 而connect.state中的内容是从context中获取的
  class Connect extends Component {
    static contextTypes = {
      store: PropTypes.object // 使用context就要声明一下
    }

    constructor () {
      super()
      this.state = { allProps: {} }
    }

    componentWillMount () {
      const { store } = this.context
      this._updateProps()
      store.subscribe(() => this._updateProps())
    }

    _updateProps () {
      const { store } = this.context
      let stateProps = mapStateToProps ? mapStateToProps(store.getState(), this.props): { } // 防止mapStateToProps没有传入
      let dispatchProps = mapDispatchToProps ? mapDispatchToProps(store.dispatch, this.props): { } // 防止mapDispatchToProps没有传入
      // 上面两行代码做了一个判断，即使只传入一个参数也不会报错，因为有的代码需要dispatch，有的可能不需要dispatch
      this.setState({
        allProps: { // 整合普通的 props 和从 state 生成的 props
          ...stateProps,
          ...dispatchProps,
          ...this.props
        }
      })
    }

    render () { // 给纯组建包了一层皮
      return <WrappedComponent {...this.state.allProps} />
    }
  }
  return Connect
}