import { connect } from 'react-redux'
import Header from '../components/Header'

// smart 组件
const mapStateToProps = (state) => {
  return {
    themeColor: state.themeColor
  }
}
export default connect(mapStateToProps)(Header)// 导出一个<Connect />组件