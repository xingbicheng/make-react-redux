import React, { Component } from 'react'

// components 里面的Header是一个Dumb组件，什么都不依赖
class Header extends Component {
  render () {
    return (
      <h1 style={{ color: this.props.themeColor }}>React.js 小书</h1>
    )
  }
}

export default Header