import React, { Component } from 'react'
import PropTypes from 'prop-types'

class ThemeSwitch extends Component {
  static propTypes = {
    themeColor: PropTypes.string,
    onSwitchColor: PropTypes.func
  }

  render () {
    return (
      <div>
        <button style={{ color: this.props.themeColor }}
                onClick={this.props.onSwitchColor.bind(this, 'red')}
                id="red">Red</button>
        <button style={{ color: this.props.themeColor }}
                onClick={this.props.onSwitchColor.bind(this, 'blue')}
                id="blue">Blue</button>
      </div>
    )
  }
}
// ThemeSwitch更新为connect返回的Connect组件
// 如果之后忘记了可以查看F12的React便懂

export default ThemeSwitch
