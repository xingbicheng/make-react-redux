import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ThemeSwitch from './ThemeSwitch'

// smart组件是可以使用smart组件的
class Content extends Component {
  static propTypes = {
    themeColor: PropTypes.string
  }

  render (){
    return (
      <div>
        <p style={{ color: this.props.themeColor }}>React.js 小书内容</p>
        <ThemeSwitch onSwitchColor={this.props.onSwitchColor.bind(this)} />
      </div>
    )
  }
}

export default Content